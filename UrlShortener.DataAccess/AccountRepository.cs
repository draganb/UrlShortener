﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UrlShortener.DataAccess.Infrastructure;
using UrlShortener.DataAccess.Models;

namespace UrlShortener.DataAccess
{
    public class AccountRepository
    {
        private IFileStore fileStore;
        private ConcurrentBag<AccountModel> data;
        private string filePath;

        public AccountRepository(string filePath)
        {
            this.filePath = filePath;
            fileStore = new JsonFileStore();
            UpdateCache();
        }

        /// <summary>
        /// Get single AccountModel
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public AccountModel Get(string accountId)
        {
            var item = data.FirstOrDefault(p => p.AccountId == accountId);
            if (item == null)
            {
                throw new ArgumentException();
            }
            return item;
        }

        /// <summary>
        /// Get all AccountModels
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AccountModel> GetAll()
        {
            return data;
        }

        /// <summary>
        /// Add new AccountModel
        /// </summary>
        /// <param name="model"></param>
        public void Add(AccountModel model)
        {
            if (data.Any(p => p.AccountId.Equals(model.AccountId, StringComparison.InvariantCultureIgnoreCase)))
            {
                throw new ArgumentException();
            }
            data.Add(model);
        }

        private void UpdateCache()
        {
            var newData = fileStore.ReadFromFile<ConcurrentBag<AccountModel>>(filePath);
            data = newData ?? new ConcurrentBag<AccountModel>();
        }

        public void DumpToFile()
        {
            fileStore.WriteToFile(filePath, data);
        }
    }
}
