﻿namespace UrlShortener.DataAccess.Infrastructure
{
    interface IFileStore
    {
        void WriteToFile<T>(string filePath, T objectToWrite) where T : new();
        void AppendToFile<T>(string filePath, T objectToWrite) where T : new();
        T ReadFromFile<T>(string filePath) where T : new();
    }
}
