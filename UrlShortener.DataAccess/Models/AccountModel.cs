﻿namespace UrlShortener.DataAccess.Models
{
    public class AccountModel
    {
        public string AccountId { get; set; }
        public string Password { get; set; }
    }
}
