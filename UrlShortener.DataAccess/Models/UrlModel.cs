﻿namespace UrlShortener.DataAccess.Models
{
    public class UrlModel
    {
        public string Code { get; set; }
        public string AccountId { get; set; }
        public string Url { get; set; }
        public int RedirectType { get; set; }
        public int CallCount { get; set; }
    }
}
