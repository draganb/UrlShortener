﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UrlShortener.DataAccess.Infrastructure;
using UrlShortener.DataAccess.Models;

namespace UrlShortener.DataAccess
{
    public class UrlRepository
    {
        private IFileStore fileStore;
        private ConcurrentBag<UrlModel> data;
        private string filePath;
        //private readonly object lockObject = new object();

        public UrlRepository(string filePath)
        {
            this.filePath = filePath;
            fileStore = new JsonFileStore();
            UpdateCache();
        }

        /// <summary>
        /// Get single UrlModel
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public UrlModel Get(string code)
        {
            var item = data.FirstOrDefault(p => p.Code == code);
            if (item == null)
            {
                throw new ArgumentException();
            }
            return item;
        }

        /// <summary>
        /// Get collection of UrlModel
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public IEnumerable<UrlModel> GetAll(string accountId = null)
        {
            var items = data.Where(p => String.IsNullOrWhiteSpace(accountId) || p.AccountId.Equals(accountId));
            return items;
        }

        /// <summary>
        /// Add UrlModel to store. Code must be unique
        /// </summary>
        /// <param name="model"></param>
        public void Add(UrlModel model)
        {
            if (data.Any(p => p.Code.Equals(model.Code, StringComparison.InvariantCultureIgnoreCase)))
            {
                throw new ArgumentException();
            }
            data.Add(model);
        }

        /// <summary>
        /// Check if code is used already
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool CodeExists(string code)
        {
            return data.Any(p => p.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Increase call count of URL
        /// </summary>
        /// <param name="code"></param>
        public void IncreaseCallCount(string code)
        {
            var item = data.FirstOrDefault(p => p.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase));

            if (item == null)
            {
                throw new ArgumentException();
            }
            lock (item)
            {
                item.CallCount++;
            }

        }

        private void UpdateCache()
        {
            var newData = fileStore.ReadFromFile<ConcurrentBag<UrlModel>>(filePath);
            data = newData;
            data = newData ?? new ConcurrentBag<UrlModel>();
        }

        public void DumpToFile()
        {
            fileStore.WriteToFile<ConcurrentBag<UrlModel>>(filePath, data);
        }

    }
}
