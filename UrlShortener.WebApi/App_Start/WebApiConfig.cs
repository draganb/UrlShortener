﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using UrlShortener.WebApi.Infrastructure;

namespace UrlShortener.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Add(typeof(IExceptionLogger), new TraceExceptionLogger());
            config.Filters.Add(new BasicAuthenticationAttribute());
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
            name: "RedirectRoute",
            routeTemplate: "{code}",
            defaults: new { controller = "Redirect", action = "Get" }
            );
        }
    }
}