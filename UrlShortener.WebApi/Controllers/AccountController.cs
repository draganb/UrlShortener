﻿using System;
using System.Web.Http;
using UrlShortener.WebApi.Infrastructure;
using UrlShortener.WebApi.Model;

namespace UrlShortener.WebApi.Controllers
{

    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        [Route("")]
        public AccountResponseModel Post([FromBody]AccountCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                return AccountResponseFactory.Get(CreateAccountResult.InvalidInputData);
            }

            return AccountService.CreateAccount(model.AccountId);
        }
    }
}
