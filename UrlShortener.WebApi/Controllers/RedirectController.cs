﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UrlShortener.WebApi.Infrastructure;

namespace UrlShortener.WebApi.Controllers
{
    public class RedirectController : ApiController
    {
        public  IHttpActionResult Get(string code)
        {
            var model = UrlService.GetSourceUrl(code);
            if (model == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            var response = Request.CreateResponse((HttpStatusCode)model.RedirectType);
            response.Headers.Location = new Uri(model.Url);
            return ResponseMessage(response);
        }
    }
}
