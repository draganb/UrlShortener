﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UrlShortener.WebApi.Infrastructure;
using UrlShortener.WebApi.Model;

namespace UrlShortener.WebApi.Controllers
{
    [RoutePrefix("register")]
    public class RegisterController : ApiController
    {
        [Route("")]
        [Authorize]
        public RegisterUrlResponseModel Post([FromBody]RegisterUrlModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = ModelState.FlattenErrors()
                });
            }

            var code = UrlService.GetCode(RequestContext.Principal.Identity.Name, model.Url, model.RedirectType);

            if (code == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }

            return new RegisterUrlResponseModel { ShortUrl = Url.Content(code) };
        }
    }
}
