﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using UrlShortener.WebApi.Infrastructure;

namespace UrlShortener.WebApi.Controllers
{
    [RoutePrefix("statistic")]
    public class StatisticController : ApiController
    {
        [Route("")]
        [Authorize]
        public Dictionary<string, int> Get([FromUri]string accountId)
        {
            /* u zadatku nije naznaceno treba li dopustiti prikaz samo vlastite statistike(autoriziranog korisnika) */

            var statistics = StatisticService.GetStatistic(accountId);

            return statistics;
        }
    }
}
