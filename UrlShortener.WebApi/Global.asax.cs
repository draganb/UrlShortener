﻿using System.Web.Hosting;
using System.Web.Http;
using UrlShortener.DataAccess;
using UrlShortener.WebApi.Infrastructure;

namespace UrlShortener.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static AccountRepository AccountRepo;
        public static UrlRepository UrlRepo;

        protected void Application_Start()
        {
            Logger.Init();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            SetupRepositories();
        }

        private void SetupRepositories()
        {
            AccountRepo = new AccountRepository(HostingEnvironment.MapPath(ConfigurationHelper.AccountsFilePath));
            UrlRepo = new UrlRepository(HostingEnvironment.MapPath(ConfigurationHelper.UrlsFilePath));
        }

        protected void Application_End()
        {
            AccountRepo.DumpToFile();
            UrlRepo.DumpToFile();
        }
    }
}