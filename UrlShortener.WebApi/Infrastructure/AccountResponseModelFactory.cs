﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrlShortener.WebApi.Model;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class AccountResponseFactory
    {
        internal static AccountResponseModel Get(CreateAccountResult result)
        {
            switch (result)
            {
                case CreateAccountResult.AccountCreated:
                    return new AccountResponseModel
                    {
                        Description = "Account created",
                        Success = true
                    };
                case CreateAccountResult.AccountIdTaken:
                    return new AccountResponseModel
                    {
                        Description = "AccountId already taken",
                        Success = false
                    };
                case CreateAccountResult.InvalidInputData:
                    return new AccountResponseModel
                    {
                        Description = "AccountId parameter is required and has to be at least 3 characters long",
                        Success = false
                    };
                case CreateAccountResult.Error:
                    return new AccountResponseModel
                    {
                        Description = "An error has occurred",
                        Success = false
                    };
                default:
                    throw new ArgumentOutOfRangeException("Unknown value for CreateAccountResult enum");
            }
        }
    }
}