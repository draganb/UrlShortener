﻿namespace UrlShortener.WebApi.Infrastructure
{
    internal enum CreateAccountResult
    {
        AccountCreated,
        AccountIdTaken,
        InvalidInputData,
        Error
    }
}