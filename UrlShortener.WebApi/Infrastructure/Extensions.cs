﻿using System.Text;
using System.Web.Http.ModelBinding;

namespace UrlShortener.WebApi.Infrastructure
{
    public static class Extensions
    {
        public static string FlattenErrors(this ModelStateDictionary input)
        {
            var sb = new StringBuilder();
            foreach (ModelState modelState in input.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    sb.Append(error.ErrorMessage);
                    sb.Append(" | ");
                }
            }
            return sb.ToString();
        }
    }
}