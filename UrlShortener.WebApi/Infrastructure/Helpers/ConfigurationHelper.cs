﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class ConfigurationHelper
    {
        internal static string AccountsFilePath
        {
            get { return ConfigurationManager.AppSettings["AccountsFilePath"]; }
        }

        internal static string UrlsFilePath
        {
            get { return ConfigurationManager.AppSettings["UrlsFilePath"]; }
        }
    }
}