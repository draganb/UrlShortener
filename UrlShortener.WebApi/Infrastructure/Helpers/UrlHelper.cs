﻿using System;
using System.Linq;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class UrlHelper
    {
        internal static string GenerateRandomCode()
        {
            var random = new Random();
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 6)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}