﻿using System;

namespace UrlShortener.WebApi.Infrastructure
{
    public static class Logger
    {
        public static void Init()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void LogFatal(this object element, Exception ex, string message = "")
        {
            GetLogger(element).Fatal(message, ex);
        }

        public static void LogFatal(this object element, string message)
        {
            GetLogger(element).Fatal(message);
        }

        public static void LogError(this object element, Exception ex, string message = "")
        {
            GetLogger(element).Error(message, ex);
        }

        public static void LogError(this object element, string message)
        {
            GetLogger(element).Error(message);
        }

        public static void LogInfo(this object element, string message)
        {
            GetLogger(element).Info(message);
        }

        public static void LogWarn(this object element, string message)
        {
            GetLogger(element).Warn(message);
        }

        private static log4net.ILog GetLogger(object element)
        {
            var typeName = (element is Type ? (Type)element : element.GetType()).FullName;
            return log4net.LogManager.GetLogger(typeName);
        }


        public static void LogError(Type type, string message)
        {
            GetLogger(type).Error(message);
        }

        public static void LogError(Type type, Exception ex, string message = "")
        {
            GetLogger(type).Error(message, ex);
        }

        public static void LogInfo(Type type, string message)
        {
            GetLogger(type).Info(message);
        }

        public static void LogWarn(Type type, string message)
        {
            GetLogger(type).Warn(message);
        }
    }
}
