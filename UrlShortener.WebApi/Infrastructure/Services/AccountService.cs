﻿using System;
using UrlShortener.DataAccess.Models;
using UrlShortener.WebApi.Model;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class AccountService
    {
        /// <summary>
        /// Create a new user account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        internal static AccountResponseModel CreateAccount(string accountId)
        {
            try
            {
                var password = PasswordHelper.Generate();
                var hash = PasswordHelper.Hash(password);

                WebApiApplication.AccountRepo.Add(new AccountModel { AccountId = accountId, Password = hash });

                var ret = AccountResponseFactory.Get(CreateAccountResult.AccountCreated);
                ret.Password = password;

                return ret;
            }
            catch(ArgumentException)
            {
                return AccountResponseFactory.Get(CreateAccountResult.AccountIdTaken);
            }
            catch(Exception ex)
            {
                Logger.LogError(typeof(AccountService), ex);
                return AccountResponseFactory.Get(CreateAccountResult.Error);
            }
        }

        /// <summary>
        /// Check if a user with supplied accountId and password exists
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        internal static bool AuthenticateUser(string accountId, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(accountId) || string.IsNullOrEmpty(password))
                {
                    return false;
                }

                var hash = PasswordHelper.Hash(password);

                var user = WebApiApplication.AccountRepo.Get(accountId);

                return user != null && user.Password.Equals(hash);
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(AccountService), ex);
                return false;
            }
        }
    }
}