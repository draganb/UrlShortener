﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class StatisticService
    {
        /// <summary>
        /// Get number of calls of each URL for given account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        internal static Dictionary<string, int> GetStatistic(string accountId)
        {
            try
            {
                var data = WebApiApplication.UrlRepo.GetAll(accountId);

                return data.GroupBy(p => p.Url).ToDictionary(p => p.Key, p => p.Sum(x => x.CallCount));
            }
            catch(Exception ex)
            {
                Logger.LogError(typeof(StatisticService), ex);
                return null;
            }
        }
    }
}