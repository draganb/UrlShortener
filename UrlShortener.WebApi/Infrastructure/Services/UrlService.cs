﻿using System;
using UrlShortener.DataAccess.Models;

namespace UrlShortener.WebApi.Infrastructure
{
    internal static class UrlService
    {
        /// <summary>
        /// Generate a code for short URL
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="url"></param>
        /// <param name="redirectType"></param>
        /// <returns></returns>
        internal static string GetCode(string accountId, string url, int redirectType)
        {
            try
            {
                string code;
                do
                {
                    code = UrlHelper.GenerateRandomCode();
                }
                while (WebApiApplication.UrlRepo.CodeExists(code));

                WebApiApplication.UrlRepo.Add(new UrlModel {AccountId = accountId, Code = code, RedirectType = redirectType, Url = url });

                return code;
            }
            catch(Exception ex)
            {
                Logger.LogError(typeof(UrlService), ex);
                return null;
            }
        }

        /// <summary>
        /// Get original URL
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        internal static UrlModel GetSourceUrl(string code)
        {
            try
            {
                var model = WebApiApplication.UrlRepo.Get(code);
                WebApiApplication.UrlRepo.IncreaseCallCount(code);

                return model;
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(UrlService), ex);
                return null;
            }
        }
    }
}