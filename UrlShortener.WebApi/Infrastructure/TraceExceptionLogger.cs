﻿using System.Web.Http.ExceptionHandling;

namespace UrlShortener.WebApi.Infrastructure
{
    public class TraceExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            Logger.LogError(typeof(WebApiApplication), context.ExceptionContext.Exception.ToString());
        }
    }
}