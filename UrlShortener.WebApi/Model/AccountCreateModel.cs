﻿using System.ComponentModel.DataAnnotations;

namespace UrlShortener.WebApi.Model
{
    public class AccountCreateModel
    {
        [Required]
        [MinLength(3)]
        public string AccountId { get; set; }
    }
}