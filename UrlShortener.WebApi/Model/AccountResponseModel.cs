﻿namespace UrlShortener.WebApi.Model
{
    public class AccountResponseModel
    {
        public bool Success { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
    }
}