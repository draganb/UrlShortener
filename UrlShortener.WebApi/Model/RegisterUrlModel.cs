﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;

namespace UrlShortener.WebApi.Model
{
    public class RegisterUrlModel
    {
        [Required]
        [Url]
        public string Url { get; set; }

        [Range(301, 302)]
        public int RedirectType { get; set; }
    }
}