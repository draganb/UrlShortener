﻿namespace UrlShortener.WebApi.Model
{
    public class RegisterUrlResponseModel
    {
        public string ShortUrl { get; set; }
    }
}